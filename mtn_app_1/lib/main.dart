import 'package:flutter/material.dart';
import 'package:mtn_app_1/dashboard_page.dart';
import 'package:mtn_app_1/edit_profile_page.dart';
import 'package:mtn_app_1/login_page.dart';
import 'package:mtn_app_1/register_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const EditProfilePage(),
      initialRoute: '/login',
      routes: {
        '/login': (context) => const LoginPage(),
        '/register': (context) => const RegisterPage(),
        '/dashboard': (context) => const DashboardPage(),
        '/editprofile': (context) => const EditProfilePage(),
      },
    );
  }
}
